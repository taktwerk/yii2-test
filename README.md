Yii2-Testaufgabe
============

ERD
---------
The schema and MySql Workbench Diagram is in the docs/ folder.

Regexp
---------
Regexp.php contains the second requirement.

Yii Setup
---------
Third requirement are contained in config/web.php and classes/User.php
