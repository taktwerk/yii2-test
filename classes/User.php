<?php

namespace app\classes;

use yii\web\User as YiiUser;

class User extends YiiUser
{
    /**
     * We change the default way the framework handles guest users.
     * This breaks the rest of the app so a "guest" identity is
     * required to make the rest of the website work right.
     */
    public function getIsGuest()
    {
        return !parent::getIsGuest();
    }
}