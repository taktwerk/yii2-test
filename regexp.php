<?php


/**
 * Rules:
 * 1) Minimum one small letter
 * 2) Minimum one capital letter
 * 3) At least one number
 * 4) At least one special character
 * 5) Starts with a letter
 * 6) Ends with a special character
 */
class PasswordValidator
{
    static protected $errors = [];

    /**
     * Determine if a password is Valid following the Taktwerk rules.
     * @param $value
     * @return bool
     */
    static public function isValid($value)
    {
        $isValid = true;

        // Upper case
        if (!preg_match('/[A-Z]/', $value)) {
            $isValid = false;
            self::logError('Password must contain at least one uppercase letter.');
        }

        // lower case
        if (!preg_match('/[a-z]/', $value)) {
            $isValid = false;
            self::logError('Password must contain at least one lowercase letter.');
        }

        // digit character
        if (!preg_match('/\d/', $value)) {
            $isValid = false;
            self::logError('Password must contain at least one digit character.');
        }

        // special character
        if (!preg_match('/[^a-z0-9]/i', $value)) {
            $isValid = false;
            self::logError('Password must contain at least one special character.');
        }

        // start with letter
        if (!preg_match('/^[a-z]/i', $value)) {
            $isValid = false;
            self::logError('Password must start with a letter.');
        }

        // end with special character
        if (!preg_match('/[^a-z0-9]$/i', $value)) {
            $isValid = false;
            self::logError('Password must end with a special character.');
        }

        return $isValid;
    }

    static public function errors()
    {
        return self::$errors;
    }

    /**
     * Get the errors
     * @param $error
     */
    static protected function logError($error)
    {
        self::$errors[] = $error;
    }
}

$password = 'bz6gg$';

if(PasswordValidator::isValid($password) === true) {
    echo 'Password is OK';
} else {
    foreach(PasswordValidator::errors() as $error) {
        echo $error.'<br />';
    }
}